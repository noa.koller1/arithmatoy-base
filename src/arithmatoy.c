#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

size_t MAX_NUMBER_DIGITS = 512;
int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }

char *arithmatoy_add(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "add: entering function\n");
  }

  char *num = NULL;
  unsigned int val = 0;
  unsigned int li = 0;
  unsigned int ri = 0;
  unsigned int ni = 0;

  lhs = drop_leading_zeros(lhs);
  rhs = drop_leading_zeros(rhs);

  li = strlen(lhs);
  ri = strlen(rhs);

  num = malloc(MAX_NUMBER_DIGITS * sizeof(char));
  memset(num, '\0', MAX_NUMBER_DIGITS * sizeof(char));

  while (li > 0 || ri > 0) {
    unsigned int carry = val;

    if (li > 0) {
      li--;
      val += get_digit_value(lhs[li]);
    }
    if (ri > 0) {
      ri--;
      val += get_digit_value(rhs[ri]);
    }

    num[ni] = to_digit(val % base);
    val /= base;

    if (VERBOSE) {
      fprintf(stderr, "add: digit %c digit %c carry %u\n",
              (li > 0 ? lhs[li] : to_digit(0)),
              (ri > 0 ? rhs[ri] : to_digit(0)),
              carry);
      fprintf(stderr, "add: result: digit %c carry %u\n", num[ni], val);
    }

    ni++;
  }

  if (val > 0) {
    if (VERBOSE) {
      fprintf(stderr, "add: final carry %u\n", val);
    }
    num[ni] = to_digit(val);
  }

  return drop_leading_zeros(reverse(num));
}

char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "sub: entering function\n");
  }

  char *num = NULL;
  int val = 0;
  unsigned int li = 0;
  unsigned int ri = 0;
  unsigned int ni = 0;

  lhs = drop_leading_zeros(lhs);
  rhs = drop_leading_zeros(rhs);

  li = strlen(lhs);
  ri = strlen(rhs);

  num = malloc(MAX_NUMBER_DIGITS * sizeof(char));
  memset(num, '\0', MAX_NUMBER_DIGITS * sizeof(char));

  while (li > 0) {
    int carry = val;

    if (ri > 0) {
      ri--;
      val += get_digit_value(rhs[ri]);
    }
    val = get_digit_value(lhs[li - 1]) - val;

    if (val < 0) {
      num[ni] = to_digit(val + base);
      val = 1;
    } else {
      num[ni] = to_digit(val);
      val = 0;
    }

    li--;

    if (VERBOSE) {
      fprintf(stderr, "sub: digit %c digit %c carry %u\n",
              (li > 0 ? lhs[li] : to_digit(0)),
              (ri > 0 ? rhs[ri] : to_digit(0)),
              carry);
      fprintf(stderr, "sub: result: digit %c carry %u\n", num[ni], val);
    }

    ni++;
  }

  if (val > 0) {
    if (VERBOSE) {
      fprintf(stderr, "sub: cannot process: %s < %s\n", lhs, rhs);
    }
    arithmatoy_free(num);
    return NULL;
  }

  return drop_leading_zeros(reverse(num));
}

char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "mul: entering function\n");
  }

  char *num = NULL;
  unsigned int val = 0;
  unsigned int li = 0;
  unsigned int ri = 0;
  unsigned int ni = 0;

  lhs = drop_leading_zeros(lhs);
  rhs = drop_leading_zeros(rhs);

  li = strlen(lhs);
  ri = strlen(rhs);

  num = malloc(MAX_NUMBER_DIGITS * sizeof(char));
  memset(num, '\0', MAX_NUMBER_DIGITS * sizeof(char));

  while (ri > 0) {
    int carry = val;
    int l = li;
    int n = ni;

    while (l > 0) {
      val += get_digit_value(lhs[l - 1]) * get_digit_value(rhs[ri - 1]);

      if (num[n] != '\0') {
        val += get_digit_value(num[n]);
      }

      num[n] = to_digit(val % base);

      val /= base;
      l--;
      n++;
    }

    if (val > 0)
      num[n] = to_digit(val);

    ri--;

    if (VERBOSE) {
      fprintf(stderr, "mul: digit %c digit %c carry %u\n",
              (li > 0 ? lhs[li] : to_digit(0)),
              (ri > 0 ? rhs[ri] : to_digit(0)),
              carry);
      fprintf(stderr, "mul: result: digit %c carry %u\n", num[ni], val);
    }

    val = 0;
    ni++;
  }

  return drop_leading_zeros(reverse(num));
}

// Utility functions

unsigned int get_digit_value(char digit) {
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return 0;
}

char to_digit(unsigned int value) {
  return get_all_digits()[value];
}

char *reverse(char *str) {
  size_t len = strlen(str);
  for (size_t i = 0; i < len / 2; ++i) {
    char tmp = str[i];
    str[i] = str[len - i - 1];
    str[len - i - 1] = tmp;
  }
  return str;
}

const char *drop_leading_zeros(const char *number) {
  while (*number == '0' && *(number + 1) != '\0') {
    ++number;
  }
  return number;
}
